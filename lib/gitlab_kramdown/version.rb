# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.29.0'
end
